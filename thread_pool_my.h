#ifndef thread_pool_my_
#define thread_pool_my_ 

#include <thread>
#include <functional>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <future>
#include <vector>
#include <atomic>
#include <type_traits>
#include <iostream>
#include <stdexcept>

using function_t = std::function<void()>;

class task_queue {
  public:
   
		bool nonblocking_pop(function_t& fun) {
      std::unique_lock<std::mutex> queue_lock{mtx_};
     
			if(!tasks_.empty())
			{
				fun = std::move(tasks_.front());	
      	tasks_.pop();
				return true;	
			}
	
			return false;
		}
		
		
		bool blocking_pop(function_t& fun) {
      std::unique_lock<std::mutex> queue_lock{mtx_};
      while ( tasks_.empty() ) {
        if ( stopped_ ) return false;
        cv_.wait(queue_lock);
      }
      fun = std::move(tasks_.front());
      tasks_.pop();
      return true;
    }

    template<typename F>
      void push(F&& f) {
        {
          std::unique_lock<std::mutex> queue_lock{mtx_};
          tasks_.emplace(std::forward<F>(f));
        }
        cv_.notify_one();
      }

    void stop() {
      {
        std::unique_lock<std::mutex> queue_lock{mtx_};
        stopped_ = true;
      }
      cv_.notify_all();
    }

  private:
    std::queue<function_t> tasks_;
    std::mutex mtx_;
    std::condition_variable cv_;
    bool stopped_ = false;
};









class thread_pool {
  public:
    thread_pool(size_t th_num = std::thread::hardware_concurrency()): 
			thread_number_{th_num},
			sched_count_{0u},
			is_started{false}
		{

			for(std::size_t i = 0; i < thread_number_; ++i)
			{
				threads_.emplace_back( std::thread{ [this,i](){ run(i); }});
   			tasks_.emplace_back(std::make_unique<task_queue>());	
			}

			is_started = true;
		}

    ~thread_pool() { 
      for(std::size_t i = 0; i < thread_number_; ++i)
				tasks_.at(i)->stop();
      for(auto &t : threads_ ) 
        t.join();
    }


		template<typename T>
		void async(T&& fun) {	
			tasks_.at(sched_count_++ % thread_number_)->push(std::forward<T>(fun));
		}



  private:
    void run(std::size_t pos) {

			while(!is_started);

			while(true) {
        function_t fun;	
				std::size_t cur_pos = pos;
				bool is_task_stolen = false;	

				do{

					if(tasks_.at(cur_pos)->nonblocking_pop(fun))
					{
						is_task_stolen = true;
						break;
					}

					++cur_pos;

					if(cur_pos == thread_number_)
						cur_pos = 0u;

				}while(cur_pos != pos);
		
				if(!is_task_stolen && !tasks_.at(pos)->blocking_pop(fun))
          return;
		
        fun();
      }
    }
		
    const size_t thread_number_;
    std::vector<std::thread> threads_;
		std::vector<std::unique_ptr<task_queue>> tasks_;
		std::atomic<unsigned long> sched_count_;
		std::atomic<bool> is_started;
};

#endif /* ifndef thread_pool_my_ */

